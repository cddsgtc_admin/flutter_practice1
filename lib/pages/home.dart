import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './imageViver.dart';

class MyHomePage2 extends StatefulWidget {
  MyHomePage2({Key key}) : super(key: key);

  @override
  _MyHomePage2State createState() => _MyHomePage2State();
}

class _MyHomePage2State extends State<MyHomePage2> {
  // 是否是最爱的
  bool _isFavorite = false;
  int _favoriteNo = 41;
  Map _iconMap = const {
    1: [Icons.phone, 'CALL'],
    2: [Icons.navigation, 'ROUTE'],
    3: [Icons.share, 'SHARE']
  };

  void _toggleFavorite() {
    setState(() {
      _isFavorite = !_isFavorite;
      if (_isFavorite)
        ++_favoriteNo;
      else
        --_favoriteNo;
    });
  }

  //block2
  Widget _getBlock2() {
    Widget star;
    if (_isFavorite) {
      star = Icon(Icons.star, size: 30, color: Colors.red);
    } else {
      star = Icon(Icons.star_border, size: 30, color: Colors.grey);
    }
    return Container(
      width: double.infinity,
      // decoration: BoxDecoration(
      //     border: Border(bottom: BorderSide(color: Colors.red[400], width: 1))),
      padding: EdgeInsets.all(32),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '今天是个好日子',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                ),
                Text(
                  '今天是个好日子，可不是吗？哈哈哈',
                  style: TextStyle(fontSize: 16, color: Colors.grey[400]),
                )
              ],
            ),
          ),
          Row(
            children: [
              GestureDetector(
                onTap: _toggleFavorite,
                child: Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: [
                      star,
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          '$_favoriteNo',
                          style: TextStyle(fontSize: 20),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  //获取icon
  Column _getIcon(int index) {
    var temp = _iconMap[index];
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 10),
          child: Icon(temp[0], color: Colors.blue[600], size: 30),
        ),
        Text(temp[1], style: TextStyle(fontSize: 18, color: Colors.blue[600])),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    //banner
    Widget banner = Container(
      width: double.infinity,
      height: 300,
      child: GestureDetector(
        child: Image(
          image: AssetImage('assets/images/st1.png'),
          fit: BoxFit.fitWidth,
        ),
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return ImageViewer(image: AssetImage('assets/images/st1.png'));
          }));
        },
      ),
    );
    //获取模块3
    Widget _block3 = Container(
      decoration: BoxDecoration(),
      padding: EdgeInsets.symmetric(vertical: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _getIcon(1),
          _getIcon(2),
          _getIcon(3),
        ],
      ),
    );
    //文字模块
    Widget _block4 = Container(
      padding: EdgeInsets.fromLTRB(20, 30, 20, 50),
      child: Center(
        child: Text(
          '   当有父Widget的配置数据改变时，同时其State.build返回的Widget结构与之前不同，此时就需要重新构建对应的Element树。为了进行Element复用，在Element重新构建前会先尝试是否可以复用旧树上相同位置的element，element节点在更新前都会调用其对应Widget的canUpdate方法，如果返回true，则复用旧Element，旧的Element会使用新Widget配置数据更新，反之则会创建一个新的Element。Widget.canUpdate主要是判断newWidget与oldWidget的runtimeType和key是否同时相等，如果同时相等就返回true，否则就会返回false。根据这个原理，当我们需要强制更新一个Widget时，可以通过指定不同的Key来避免复用。当有父Widget的配置数据改变时，同时其State.build返回的Widget结构与之前不同，此时就需要重新构建对应的Element树。为了进行Element复用，在Element重新构建前会先尝试是否可以复用旧树上相同位置的element，element节点在更新前都会调用其对应Widget的canUpdate方法，如果返回true，则复用旧Element，旧的Element会使用新Widget配置数据更新，反之则会创建一个新的Element。Widget.canUpdate主要是判断newWidget与oldWidget的runtimeType和key是否同时相等，如果同时相等就返回true，否则就会返回false。根据这个原理，当我们需要强制更新一个Widget时，可以通过指定不同的Key来避免复用。',
          style: TextStyle(
            fontSize: 16,
            height: 2,
          ),
        ),
      ),
    );

    return Scaffold(
      body: Container(
        color: Colors.white,
        child: ListView(
          children: [
            banner,
            _getBlock2(),
            _block3,
            _block4,
          ],
        ),
      ),
    );
  }
}
