import 'package:flutter/material.dart';
import "package:photo_view/photo_view.dart";

///图片浏览器
class ImageViewer extends StatelessWidget {
  const ImageViewer({Key key, this.image}) : super(key: key);
  final ImageProvider image;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        color: Colors.black,
        child: Center(
          child: PhotoView(
            maxScale: 2.5,
            minScale: .8,
            imageProvider: image,
          ),
        ),
      ),
    );
  }
}
