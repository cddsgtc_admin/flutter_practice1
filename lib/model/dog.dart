// import 'dart:html';

/* import 'package:flutter/material.dart';

class Dog extends InheritedWidget {
  Dog({Key key, this.child}) : super(key: key, child: child);

  final Widget child;

  String name, age, gender, category, birthday, host;

  static Dog of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<Dog>();
  }

  @override
  bool updateShouldNotify(Dog oldWidget) {
    return true;
  }
} */

class Dog {
  String name, gender, category, host;
  DateTime birthday;

  Dog({this.name, this.gender, this.category, this.host, this.birthday});

  ///获取年龄
  num get age {
    if (birthday != null) {
      return DateTime.now().difference(birthday).inDays / 365;
    } else {
      return 0;
    }
  }
}
