import 'package:flutter/foundation.dart';
import '../calculator/Store.dart';

///数据
class CData extends ChangeNotifier {
  final Store store = Store();
  InputStr transStr;

  ///设置选中的结果表达式
  void setTransStr(InputStr str) {
    transStr = str;
    notifyListeners();
  }

  /// 添加一个表达式
  void addStr(List<InputStr> strs) async {
    await store.addData(strs);
    notifyListeners();
  }

  ///清除选中的表达式
  void clearTransStr() {
    transStr = null;
    notifyListeners();
  }

  ///清除所有保存的文字
  void clearStore() async {
    await store.clear();
    notifyListeners();
  }
}
