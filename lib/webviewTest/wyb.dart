import 'dart:io';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter/material.dart';

class Wyb extends StatefulWidget {
  Wyb({Key key}) : super(key: key);

  @override
  _WybState createState() => _WybState();
}

class _WybState extends State<Wyb> {
  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.teal[50],
        padding: EdgeInsets.only(top: 24),
        child: WebView(
          // initialUrl: 'https://www.baidu.com',
          initialUrl: 'http://m.chinamedins.com',
          javascriptMode: JavascriptMode.unrestricted,
          onPageStarted: (err) {
            print('下载页面,$err');
          },
          onWebResourceError: (name) {
            print('页面出错');
          },
        ),
      ),
    );
  }
}
