// import 'package:cdd_f_1/main1.dart';
// import 'package:cdd_f_1/pages/home.dart';
import 'package:cdd_f_1/model/caculate.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'calculator/home.dart';
import './webviewTest/wyb.dart';
import './model/caculate.dart';

void main() {
  runApp(MyApp());
}

/* class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '第一个测试app',
      home: MyHomePage2(),
    );
  }
} */

///计算器部分
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: Theme.of(context).primaryColor, //默认背景
      theme: ThemeData(
        //主题相关
        primaryColor: Colors.blueGrey,
      ),
      title: '第一个测试app',
      // home: Wyb(), //我医保webview
      home: ChangeNotifierProvider(
        create: (context) => CData(),
        child: CalculatorHome(),
      ), //计算器
    );
  }
}
