import 'dart:convert';

import 'package:flutter/material.dart';
import './Store.dart';

Store store;

class History extends StatefulWidget {
  History({Key key}) : super(key: key);

  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  @override
  Widget build(BuildContext _) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 50,
          title: Text('记录'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pop(_);
            },
          ),
          actions: [
            FlatButton(
              onPressed: () {
                _clearHistory().then((res) {
                  if (res == null) {
                    print('取消清除');
                  } else {
                    print('确认清除');
                  }
                });
                // Store()
                //   ..clear().then((name) {
                //     setState(() {});
                //   });
              },
              child: Text(
                '清除',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
        body: HistoryList(),
      ),
    );
  }

  Future<bool> _clearHistory() async {
    return showDialog<bool>(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text('警告'),
            content: Text('您确定要清楚历史记录吗？'),
            actions: [
              FlatButton(
                child: Text('取消'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text(
                  '删除',
                  style: TextStyle(color: Colors.red),
                ),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
            ],
          );
        });
  }
}

class HistoryList extends StatefulWidget {
  HistoryList({Key key}) : super(key: key);
  @override
  _HistoryListState createState() => _HistoryListState();
}

class _HistoryListState extends State<HistoryList> {
  bool onLoading = false;
  String str = '';
  List<InputStr> history = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      onLoading = true;
    });
    if (store == null) store = Store();
    store.getAll().then((res) {
      setState(() {
        history = res;
        onLoading = false;
        print('history is ${jsonEncode(history)}');
        print('histor length is ${history.length}');
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    //正常组件
    Widget list = ListView.builder(
      itemCount: history.length,
      itemBuilder: (context, index) {},
    );
    //loading组件
    Widget _loading = Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 60,
            width: 60,
            child: CircularProgressIndicator(),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: Text('loading...',
                style: TextStyle(color: Theme.of(context).primaryColor)),
          )
        ],
      ),
    );
    return Container(
      child: onLoading ? _loading : Text(str),
    );
  }
}
